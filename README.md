# NewRelic Module

## Maintainer Contact

 * Jared Kipe
   <jared (at) jaredkipe (dot) com>

## Requirements

SilverStripe 3.1 jaredkipe/silverstripe-framework
  https://github.com/silverstripe/silverstripe-framework/pull/3355
NewRelic PHP or HHVM extension installed and
  configured with your NR API key.
  https://github.com/jaredkipe/hhvm-newrelic-ext/tree/automatic-segments

## Documentation

This module provides better transaction naming when using NewRelic PHP or HHVM extensions.


## Configuring

After installing this module you will need to enable it, and optionally
provide a transaction base (useful when running multiple SilverStripe
sites on the same physical host).

*mysite/_config/newrelic.yml*

	---
	name: newrelic
	---
	NewRelicRequestExtension:
      enabled: 1
      transaction_base: MySite

