<?php
/**
 *	NewRelicRequestExtension currently needs call from owner's 
 *	->extend('beforeCallActionHandler', $request, $action)
 */
class NewRelicRequestExtension extends Extension {
    public function beforeCallActionHandler($request, $action) {
        $enabled = Config::inst()->get('NewRelicRequestExtension', 'enabled');
        if (extension_loaded('newrelic') && $enabled) {
            $base = Config::inst()->get('NewRelicRequestExtension', 'transaction_base');
            newrelic_name_transaction($base . '/' . $this->getOwner() . '/' . $action);
        } else if ($enabled) {
            SS_Log::log('SilverStripe NewRelic enabled, but extension not loaded', SS_Log::WARN);
        }
    }
}